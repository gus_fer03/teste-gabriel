import pygame

class Tower:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.height = 0
        self.width = 0

    def loadSprite(self, img, window):
        window.blit(img, (self.x, self.y))