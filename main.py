import pygame
from random import randint
from PPlay.keyboard import *
from PPlay.window import *
from PPlay.gameimage import *

# Módulos próprios
from torres import *

# Carregar a janela
window = Window(1067, 600)
background = GameImage("assets/mapa.jpg")
pygameWindow = window.get_screen()

keyboard = window.get_keyboard()

towers = []
buttonPressed = False

while True:
    background.draw()
    clock = window.delta_time()

    mouse = pygame.mouse

    # Verifica se foi pressionado algum botão para colocar torre
    if keyboard.key_pressed("1"):
        towers.append(WhitePawn(*mouse.get_pos()))
    elif keyboard.key_pressed("2"):
        towers.append(WhiteKnight(*mouse.get_pos()))
    elif keyboard.key_pressed("3"):
        towers.append(WhiteBishop(*mouse.get_pos()))
    elif keyboard.key_pressed("4"):
        towers.append(WhiteTower(*mouse.get_pos()))
    elif keyboard.key_pressed("5"):
        towers.append(WhiteQueen(*mouse.get_pos()))

    # Carrega os sprites das torres
    for tower in towers:
        tower.loadSprite(pygameWindow)

    window.update()