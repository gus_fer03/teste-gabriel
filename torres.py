from classes import Tower
import pygame
import os

# Peão
class WhitePawn(Tower):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.img = pygame.image.load(os.path.join("assets/branco", "branco-peao.png"))

    def loadSprite(self, window):
        super().loadSprite(self.img, window)

# Cavalo
class WhiteKnight(Tower):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.img = pygame.image.load(os.path.join("assets/branco", "branco-cavalo.png"))

    def loadSprite(self, window):
        super().loadSprite(self.img, window)

# Bispo
class WhiteBishop(Tower):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.img = pygame.image.load(os.path.join("assets/branco", "branco-bispo.png"))

    def loadSprite(self, window):
        super().loadSprite(self.img, window)

# Torre
class WhiteTower(Tower):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.img = pygame.image.load(os.path.join("assets/branco", "branco-torre.png"))

    def loadSprite(self, window):
        super().loadSprite(self.img, window)

# Rainha
class WhiteQueen(Tower):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.img = pygame.image.load(os.path.join("assets/branco", "branco-rainha.png"))

    def loadSprite(self, window):
        super().loadSprite(self.img, window)